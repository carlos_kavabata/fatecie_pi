/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.empresa.servico;

import br.com.empresa.entidade.Cidade;
import br.com.empresa.entidade.Estado;
import br.com.empresa.utils.Transacional;
import java.io.Serializable;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author ricardo
 */
@Transacional
public class CidadeServico extends AbstractServico<Cidade> implements Serializable{
    @Inject
    private EntityManager em;

    public CidadeServico() {
        super(Cidade.class);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }
    
}
