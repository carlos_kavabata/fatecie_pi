/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.empresa.servico;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author ricardo
 * @param <T>
 */
public abstract class AbstractServico<T> implements Serializable{

    private final Class<T> classe;

    public AbstractServico(Class<T> classe) {
        this.classe = classe;
    }

    public abstract EntityManager getEm();

    public T salvar(T entidade) {
        entidade = getEm().merge(entidade);
        return entidade;
    }

    public void excluir(T entidade) {
        getEm().remove(getEm().merge(entidade));
    }

    public T pesquisar(Object id) {
        T entidade = getEm().find(classe, id);
        return entidade;
    }

    public List<T> listar() {
        return getEm().createQuery("FROM " + classe.getSimpleName()).getResultList();
    }

}
