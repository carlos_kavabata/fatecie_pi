/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.empresa.servico;

import br.com.empresa.entidade.Cliente;
import br.com.empresa.utils.Transacional;
import java.io.Serializable;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author ricardo
 */
@Transacional
public class ClienteServico extends AbstractServico<Cliente> implements Serializable{
    @Inject
    private EntityManager em;

    public ClienteServico() {
        super(Cliente.class);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }
    
}
