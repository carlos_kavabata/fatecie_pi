/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.empresa.controle;

import br.com.empresa.converter.ConverterGenerico;
import br.com.empresa.entidade.Cidade;
import br.com.empresa.servico.CidadeServico;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author ricardo
 */
@Named
@ViewScoped
public class CidadeControle implements Serializable {

    private Cidade cidade;
    private boolean viewList = true;
    private boolean viewForm = false;
    @Inject
    private CidadeServico cidadeServico;
    private ConverterGenerico converterGenerico;
    
    public ConverterGenerico converter(){
        if(converterGenerico == null){
            converterGenerico = new ConverterGenerico(cidadeServico);
        }
        return converterGenerico;
    }

    public void criaCidade() {
        try {
            cidade = new Cidade();
            viewList = false;
            viewForm = true;
        } catch (Exception ex) {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    ex.getMessage(), "");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    public void alterar() {
        viewList = false;
        viewForm = true;
    }

    public String salvar() {
        cidade = cidadeServico.salvar(cidade);
        return "listagem?faces-redirect=true";
    }

    public void excluir() {
        cidadeServico.excluir(cidade);
    }

    public List<Cidade> getListaCidade() {
        return cidadeServico.listar();
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade estado) {
        this.cidade = estado;
    }

    public boolean getViewList() {
        return viewList;
    }

    public void setViewList(boolean viewList) {
        this.viewList = viewList;
    }

    public boolean getViewForm() {
        return viewForm;
    }

    public void setViewForm(boolean viewForm) {
        this.viewForm = viewForm;
    }

}
