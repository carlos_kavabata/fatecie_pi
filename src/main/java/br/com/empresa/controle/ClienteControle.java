/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.empresa.controle;

import br.com.empresa.entidade.Cliente;
import br.com.empresa.entidade.PessoaFisica;
import br.com.empresa.entidade.PessoaJuridica;
import br.com.empresa.servico.ClienteServico;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author ricardo
 */
@Named
@ViewScoped
public class ClienteControle implements Serializable {

    private Cliente cliente;
    private String tipoPessoa = "PF";
    private boolean viewList = true;
    private boolean viewForm = false;
    @Inject
    private ClienteServico clienteServico;

    public void criaCliente() {
        try {
            viewList = false;
            viewForm = true;
            if (tipoPessoa.equals("PF")) {
                cliente = new Cliente(new PessoaFisica());
            } else {
                cliente = new Cliente(new PessoaJuridica());
            }
        } catch (Exception ex) {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    ex.getMessage(), "");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    public void alterar() {
        viewList = false;
        viewForm = true;
        if (cliente.getPessoa() instanceof PessoaFisica) {
            tipoPessoa = "PF";
        } else {
            tipoPessoa = "PJ";
        }
    }

    public String salvar() {
        cliente = clienteServico.salvar(cliente);
        return "listagem?faces-redirect=true";
    }

    public void excluir() {
        clienteServico.excluir(cliente);
    }

    public List<Cliente> getListaCliente() {
        return clienteServico.listar();
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public boolean getViewList() {
        return viewList;
    }

    public void setViewList(boolean viewList) {
        this.viewList = viewList;
    }

    public boolean getViewForm() {
        return viewForm;
    }

    public void setViewForm(boolean viewForm) {
        this.viewForm = viewForm;
    }

    }
