/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.empresa.controle;

import br.com.empresa.converter.ConverterGenerico;
import br.com.empresa.entidade.Estado;
import br.com.empresa.servico.EstadoServico;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author ricardo
 */
@Named
@ViewScoped
public class EstadoControle implements Serializable {

    private Estado estado;
    private boolean viewList = true;
    private boolean viewForm = false;
    @Inject
    private EstadoServico estadoServico;
    private ConverterGenerico converterGenerico;
    
    public ConverterGenerico converter(){
        if(converterGenerico == null){
            converterGenerico = new ConverterGenerico(estadoServico);
        }
        return converterGenerico;
    }

    public void criaEstado() {
        try {
            estado = new Estado();
            viewList = false;
            viewForm = true;
        } catch (Exception ex) {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    ex.getMessage(), "");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    public void alterar() {
        viewList = false;
        viewForm = true;
    }

    public String salvar() {
        estado = estadoServico.salvar(estado);
        return "listagem?faces-redirect=true";
    }

    public void excluir() {
        estadoServico.excluir(estado);
    }

    public List<Estado> getListaEstado() {
        return estadoServico.listar();
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public boolean getViewList() {
        return viewList;
    }

    public void setViewList(boolean viewList) {
        this.viewList = viewList;
    }

    public boolean getViewForm() {
        return viewForm;
    }

    public void setViewForm(boolean viewForm) {
        this.viewForm = viewForm;
    }

}
