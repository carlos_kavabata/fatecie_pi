/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.empresa.converter;

import br.com.empresa.servico.AbstractServico;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author ricardo
 */
public class ConverterGenerico implements Converter {

    private final AbstractServico abstractServico;

    public ConverterGenerico(AbstractServico abstractServico) {
        this.abstractServico = abstractServico;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return abstractServico.pesquisar(Long.valueOf(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value.toString();
    }

}
