/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.empresa.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author ricardo
 */
@Entity
@Table(name = "pessoafisica")
public class PessoaFisica extends Pessoa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "pes_cpf", unique = true, nullable = false)
    private String cpf;
    @Column(name = "pes_rg")
    private String rg;
    @Column(name = "pes_nascimento")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date nascimento;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
        this.nascimento = nascimento;
    }
    
    
}
